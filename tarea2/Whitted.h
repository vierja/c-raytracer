#ifndef WHITTED_H
#define WHITTED_H

#include "Color.h"
#include "Vector.h"
#include "Definiciones.h"
#include "Objeto.h"
#include "Lista.h"
#include "Luz.h"
#include "Rayo.h"

Color* traza_RR(Rayo* rayo, Lista* listaObjetos, Lista* listaLuces, int profundidad, int maxProfundidad, Color* luzAmbiental, bool verReflexion, bool verTransmision);
Color* sombra_RR(Objeto* objeto, Rayo* rayo, Punto* punto, Vector* normal, int profundidad, int maxProfundidad, Lista* listaObjetos, Lista* luces, Color* luzAmbiental);
double calcularPorcentajeLuz(Rayo* rayoPuntoLuz, Lista* listaObjetos);

#endif