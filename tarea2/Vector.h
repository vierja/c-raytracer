#ifndef VECTOR_H
#define VECTOR_H

#include "Cuaternion.h"

#include <math.h>

class Punto;

class Vector {
	public:
		Punto* origen;
		Punto* destino;
		Punto* resta;

		Vector(Punto *fin);	//Vector del origen a fin
		Vector(Punto* origen, Punto* destino);
		Vector(Vector* copiame);
		Vector(Vector* a, Vector* b); //Suma
		~Vector();

		float productoEscalar(Vector* otro);
		Vector* productoVectorial(Vector *otro);
		float modulo();
		void trasladarACero();
		void normalizar();
		float angulo(Vector* v);
		void darVuelta();

		Vector* calcularVectorReflexion(Vector* normal);
		Vector* calcularVectorTransmision(Vector* normal, float indRefOrigen, float indRefDestino);

};

#endif