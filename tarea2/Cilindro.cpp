#include "Cilindro.h"
#include <stdlib.h>

Cilindro::Cilindro(int id, Punto* centroBase, float radio, float altura, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion){
	this->id = id;
	this->centroBase = centroBase;
	this->radio  = radio;
	this->altura = altura;
	this->color  = color;
	this->colorEspecular = colorEspecular;
	this->cantLuzAmbiental = cantLuzAmbiental;
	this->cantLuzDifusa = cantLuzDifusa;
	this->coefEspecular = coefEspecular;
	this->exponReflexionEspecular = exponReflexionEspecular;
	this->coefTransmision = coefTransmision;
	this->indiceRefraccion = indiceRefraccion;
};

int Cilindro::getId(){ return id; };

TipoObjeto Cilindro::getTipoObjeto(){
	return CILINDRO;
};

Color* Cilindro::getColor(){
	return this->color;
};
Color* Cilindro::getColorEspecular(){
	return this->colorEspecular;
};
float Cilindro::getCantLuzAmbiental(){
	return this->cantLuzAmbiental;
};
float Cilindro::getCantLuzDifusa(){
	return this->cantLuzDifusa;
};
float Cilindro::getCoefEspecular(){
	return this->coefEspecular;
};
float    Cilindro::getExponReflexionEspecular(){
	return this->exponReflexionEspecular;
};
float Cilindro::getCoefTransmision(){
	return this->coefTransmision;
};
float Cilindro::getIndiceRefraccion(){
	return this->indiceRefraccion;
};
Vector* Cilindro::normal (Punto* punto){
	if((punto->y - this->centroBase->y) < MINIMO_ERROR) {
		Punto* aux = new Punto(punto);
		aux->sumarPunto(new Punto(0,-1,0));
		return new Vector(punto, aux);
	} else if((punto->y - (this->centroBase->y + this->altura)) < MINIMO_ERROR) {
		Punto* aux = new Punto(punto);
		aux->sumarPunto(new Punto(0,1,0));
		return new Vector(punto, aux);
	} else {	// El punto esta en la pared lateral
		Vector* auxVec = new Vector(new Punto(this->centroBase->x, punto->y, this->centroBase->z), punto);
		Punto* aux = new Punto(punto);
		aux->sumarPunto(auxVec->resta);
		return new Vector(punto, aux);
	}
};


Interseccion* Cilindro::intersectaRayo(Rayo* rayoR){
	Vector* rayo = rayoR->ray;
	// Interseccion con costado de cilindro.
	Punto* interLadoUno = NULL;
	Punto* interLadoDos = NULL;
	// Se busca la interseccion del rayo con (z-h)^2 + (x-k)^2 = r^2, (h,k)= centroBase, r=radio.
	// Primero se chequea que lo que en el paso siguiente estara adentro de la raiz en el calculo de lambda sea positivo.
	//Linea.
	float a = rayo->origen->x;
	float b = rayo->origen->y;
	float c = rayo->origen->z;
	float q = rayo->resta->x;
	float w = rayo->resta->y;
	float e = rayo->resta->z;
	//Centro.
	float h = this->centroBase->z;
	float k = this->centroBase->x;
	//Radio.
	float r = this->radio;
	float check = -4*(-pow(r,2) + pow(c-h,2) + pow(a-k,2))*(pow(e,2) + pow(q,2)) + pow(2*e*(c-h) + 2*q*(a-k),2);
	if(check >= 0) {
		float lambda = -(sqrt(check) + 2*q*(a-k) + 2*e*(c-h)) / (2*pow(q,2) + 2*pow(e,2));
		interLadoUno = new Punto(a+lambda*q, b+lambda*w, c+lambda*e);
		if((interLadoUno->y <= this->centroBase->y + MINIMO_ERROR) || (interLadoUno->y + MINIMO_ERROR >= (this->centroBase->y + this->altura))) {
			interLadoUno = NULL;
		}
		lambda = (sqrt(check) - 2*q*(a-k) - 2*e*(c-h)) / (2*pow(q,2) + 2*pow(e,2));
		interLadoDos = new Punto(a+lambda*q, b+lambda*w, c+lambda*e);
		if((interLadoDos->y <= this->centroBase->y + MINIMO_ERROR) || (interLadoDos->y + MINIMO_ERROR >= (this->centroBase->y + this->altura))) {
			interLadoDos = NULL;
		}
	}

// Interseccion con tapas de cilindro.
	Punto* interBaseBaja = NULL;
	Punto* interBaseAlta = NULL;
	// Se usa el 'y' de la tapa para calcular lambda utilizando la ecuacion de la recta y luego se calculan las demas coordenadas de la interseccion.
	// Primero se cheque que el rayo no sea paralelo al plano de la tapa.
	if(rayo->resta->y != 0) {
		float lambda = (this->centroBase->y - b) / w;
		interBaseBaja = new Punto(a+lambda*q, this->centroBase->y, c+lambda*e);
		if(interBaseBaja->distancia(this->centroBase) > this->radio) {
			interBaseBaja = NULL;
		}
		lambda = ((this->centroBase->y+this->altura) - b) / w;
		interBaseAlta = new Punto(a+lambda*q, this->centroBase->y+this->altura, c+lambda*e);
		if(interBaseAlta->distancia(this->centroBase) > this->radio) {
			interBaseAlta = NULL;
		}
	}

	if (this->id == rayoR->idObjetoOrigen){
		if (interLadoUno != NULL){
			if (interLadoUno->igual(rayoR->ray->origen)){
				interLadoUno = NULL;
			}
		}
		if (interLadoDos != NULL){
			if (interLadoDos->igual(rayoR->ray->origen)){
				interLadoDos = NULL;
			}
		}
		if (interBaseAlta != NULL){
			if (interBaseAlta->igual(rayoR->ray->origen)){
				interBaseAlta = NULL;
			}
		}
		if (interBaseBaja != NULL){
			if (interBaseBaja->igual(rayoR->ray->origen)){
				interBaseBaja = NULL;
			}
		}
	}
	//Se chequea que el punto encontrado esta en la direccion del rayo.
	if (interLadoUno != NULL){
		if (!X_mayorA_Y(((interLadoUno->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
			interLadoUno = NULL;
		}
	}
	if (interLadoDos != NULL){
		if (!X_mayorA_Y(((interLadoDos->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
			interLadoDos = NULL;
		}
	}
	if (interBaseAlta != NULL){
		if (!X_mayorA_Y(((interBaseAlta->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
			interBaseAlta = NULL;
		}
	}
	if (interBaseBaja != NULL){
		if (!X_mayorA_Y(((interBaseBaja->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
			interBaseBaja = NULL;
		}
	}

	// Se busca la interseccion mas cercana al observador.
	float distanciaMin = -1;
	Punto *interPosta = interLadoUno;
	//DEBUG
	if (interLadoUno != NULL && interLadoDos != NULL) {
		interLadoUno = interLadoUno;
		//ESTE CASO DEJA MEMORIA COLGADA.
	}
	if(interLadoUno != NULL) {
		distanciaMin = interLadoUno->distancia(rayo->origen);
	}
	if(interLadoDos != NULL) {
		float distAux = interLadoDos->distancia(rayo->origen);
		if(distanciaMin == -1) {
			distanciaMin = distAux;
			interPosta = interLadoDos;
		} else if(distanciaMin > distAux) {
			delete interPosta;
			distanciaMin = distAux;
			interPosta = interLadoDos;
		}
	}
	if(interBaseBaja != NULL) {
		float distAux = interBaseBaja->distancia(rayo->origen);
		if(distanciaMin == -1) {
			distanciaMin = distAux;
			interPosta = interBaseBaja;
		} else if(distanciaMin > distAux) {
			delete interPosta;
			distanciaMin = distAux;
			interPosta = interBaseBaja;
		}
	}
	if(interBaseAlta != NULL) {
		float distAux = interBaseAlta->distancia(rayo->origen);
		if(distanciaMin == -1) {
			distanciaMin = distAux;
			interPosta = interBaseAlta;
		} else if(distanciaMin > distAux) {
			delete interPosta;
			distanciaMin = distAux;
			interPosta = interBaseAlta;
		}
	}
	if(interPosta != NULL) {
		/*if (interPosta != interLadoUno){
			delete interLadoUno;
		}
		if (interPosta != interLadoDos){
			delete interLadoDos;
		}
		if (interPosta != interBaseAlta){
			delete interBaseAlta;
		}
		if (interPosta != interBaseBaja){
			delete interBaseBaja;
		}*/
		return new Interseccion(interPosta, this);
	} else {
		return NULL;
	}
}