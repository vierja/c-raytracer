#ifndef CILINDRO_H
#define CILINDRO_H

#include "Punto.h"
#include "Color.h"
#include "Objeto.h"
#include "Definiciones.h"

#include "Math.h"


class Cilindro: public Objeto{
public:
	Punto* centroBase;
	float radio;
	float altura;
	int id;

	//int planoBase;

	Color* color;
	Color* colorEspecular;

	float cantLuzAmbiental;
	float cantLuzDifusa;
	float coefEspecular;
	float	   exponReflexionEspecular;
	float coefTransmision;
	float indiceRefraccion;

	Cilindro(int id, Punto* centroBase, float radio, float altura, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion);
	
	TipoObjeto getTipoObjeto();
	int getId();
	Color* getColor();
	Color* getColorEspecular();
	float getCantLuzAmbiental();
	float getCantLuzDifusa();
	float getCoefEspecular();
	float getExponReflexionEspecular();
	Vector* normal (Punto* punto);
	float getCoefTransmision();
	float getIndiceRefraccion();
	Interseccion* intersectaRayo(Rayo* rayoR);
};

#endif