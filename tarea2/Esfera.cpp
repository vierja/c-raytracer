#include "Esfera.h"
#include <stdlib.h>

Esfera::Esfera(int id,Punto* centro, float radio, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion){
	this->id = id;
	this->centro = centro;
	this->radio  = radio;
	this->color  = color;
	this->colorEspecular = colorEspecular;
	this->cantLuzAmbiental = cantLuzAmbiental;
	this->cantLuzDifusa = cantLuzDifusa;
	this->coefEspecular = coefEspecular;
	this->exponReflexionEspecular = exponReflexionEspecular;
	this->coefTransmision = coefTransmision;
	this->indiceRefraccion = indiceRefraccion;
};

int Esfera::getId(){ return id; };
TipoObjeto Esfera::getTipoObjeto(){
	return ESFERA;
};

Color* Esfera::getColor(){
	return this->color;
};
Color* Esfera::getColorEspecular(){
	return this->colorEspecular;
};
float Esfera::getCantLuzAmbiental(){
	return this->cantLuzAmbiental;
};
float Esfera::getCantLuzDifusa(){
	return this->cantLuzDifusa;
};
float Esfera::getCoefEspecular(){
	return this->coefEspecular;
};
float Esfera::getExponReflexionEspecular(){
	return this->exponReflexionEspecular;
};
float Esfera::getCoefTransmision(){
	return this->coefTransmision;
};
float Esfera::getIndiceRefraccion(){
	return this->indiceRefraccion;
};
Vector* Esfera::normal (Punto* punto){
	Vector* auxVec = new Vector(this->centro, punto);
	Punto* aux = new Punto(punto);
	aux->sumarPunto(auxVec->resta);
	return new Vector(punto, aux);
};

Interseccion* Esfera::intersectaRayo(Rayo* rayo){
	// Se busca la interseccion del rayo con (x-h)^2 + (y-k)^2 + (z-j)^2 = r^2, (h,k,j)= centroBase, r=radio.
	Punto* interUno = NULL;
	Punto* interDos = NULL;
	//Linea.
	float a = rayo->ray->origen->x;
	float b = rayo->ray->origen->y;
	float c = rayo->ray->origen->z;
	float q = rayo->ray->resta->x;
	float w = rayo->ray->resta->y;
	float e = rayo->ray->resta->z;
	//Esfera.
	float h = this->centro->x;
	float k = this->centro->y;
	float j = this->centro->z;
	float r = this->radio;

	// Primero se chequea que lo que esta dentro de la raiz en la resolucion de la ecuacion sea positivo.
	float check = -4*(-pow(r,2) + pow(a-h,2) + pow(b-k,2) + pow(c-j,2)) * (pow(e,2) + pow(q,2) + pow(w,2)) + pow(2*e*(c-j) + 2*q*(a-h) + 2*w*(b-k),2);
	if(check >= 0) {
		float lambda = -(sqrt(check) + 2*q*(a-h) + 2*w*(b-k) + 2*e*(c-j)) / (2*pow(q,2) + 2*pow(w,2) + 2*pow(e,2));
		interUno = new Punto(a+lambda*q, b+lambda*w, c+lambda*e);
		lambda = (sqrt(check) - 2*q*(a-h) - 2*w*(b-k) - 2*e*(c-j)) / (2*pow(q,2) + 2*pow(w,2) + 2*pow(e,2));
		interDos = new Punto(a+lambda*q, b+lambda*w, c+lambda*e);
	}

	if (this->id == rayo->idObjetoOrigen){
		if (interUno != NULL){
			if (interUno->igual(rayo->ray->origen)){
				interUno = NULL;
			}
		}
		if (interDos != NULL){
			if (interDos->igual(rayo->ray->origen)){
				interDos = NULL;
			}
		}
	}

	if (interUno != NULL){
		if (!X_mayorA_Y(((interUno->x - rayo->ray->origen->x)/(rayo->ray->resta->x)) , 0)){
			interUno = NULL;
		}
	}
	if (interDos != NULL){
		if (!X_mayorA_Y(((interDos->x - rayo->ray->origen->x)/(rayo->ray->resta->x)) , 0)){
			interDos = NULL;
		}
	}

	Punto* interPosta = interUno;
	if(interUno == NULL) {
		interPosta = interDos;
	} else {
		if(interDos != NULL) {
			if(interUno->distancia(rayo->ray->origen) > interDos->distancia(rayo->ray->origen)) {
				interPosta = interDos;
			}
		}
	}

	if(interPosta == NULL) {
		return NULL;
	} else {
		if (interUno != interPosta){
			delete interUno;
		}
		if (interDos != interPosta){
			delete interDos;
		}
		return new Interseccion(interPosta, this);
	}
}