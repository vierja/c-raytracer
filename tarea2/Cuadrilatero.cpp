#include "Cuadrilatero.h"

Cuadrilatero::Cuadrilatero(int id,Punto* pA, Punto* pB, Punto* pC, Punto* pD, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, int exponReflexionEspecular, float coefTransmision, float indiceRefraccion){
	this->id = id;
	this->pA = pA;
	this->pB  = pB;
	this->pC = pC;
	this->pD = pD;
	Vector *BA = new Vector(pA, pB);
	Vector *DA = new Vector(pA, pD);
	this->norm = BA->productoVectorial(DA);
	this->color  = color;
	this->colorEspecular = colorEspecular;
	this->cantLuzAmbiental = cantLuzAmbiental;
	this->cantLuzDifusa = cantLuzDifusa;
	this->coefEspecular = coefEspecular;
	this->exponReflexionEspecular = exponReflexionEspecular;
	this->coefTransmision = coefTransmision;
	this->indiceRefraccion = indiceRefraccion;
}

int Cuadrilatero::getId(){ return id; };

TipoObjeto Cuadrilatero::getTipoObjeto(){
	return CUADRILATERO;
};

Color* Cuadrilatero::getColor(){
	return this->color;
};
Color* Cuadrilatero::getColorEspecular(){
	return this->colorEspecular;
};
float Cuadrilatero::getCantLuzAmbiental(){
	return this->cantLuzAmbiental;
};
float Cuadrilatero::getCantLuzDifusa(){
	return this->cantLuzDifusa;
};
float Cuadrilatero::getCoefEspecular(){
	return this->coefEspecular;
};
float    Cuadrilatero::getExponReflexionEspecular(){
	return this->exponReflexionEspecular;
};
float Cuadrilatero::getCoefTransmision(){
	return this->coefTransmision;
};
float Cuadrilatero::getIndiceRefraccion(){
	return this->indiceRefraccion;
};

Interseccion* Cuadrilatero::intersectaRayo(Rayo* rayoR){
	Vector* rayo = rayoR->ray;
	// Se chequea que el rayo no sea perpendicular al vector normal al plano.
	float cosNormRayo = this->norm->productoEscalar(rayo) / (this->norm->modulo() * rayo->modulo());
	if(!X_igualA_Y(cosNormRayo,0)) {
		// Se calcula el plano que contiene al cuadrilatero (Ax+By+Cz+D=0) a partir del vector normal y un punto.
		float A = this->norm->resta->x;
		float B = this->norm->resta->y;
		float C = this->norm->resta->z;
		float D = -(A*this->pA->x + B*this->pA->y + C*this->pA->z);
		float lambda = - ((D+A*rayo->origen->x+B*rayo->origen->y+C*rayo->origen->z)/(A*rayo->resta->x+B*rayo->resta->y+C*rayo->resta->z));
		// Se utiliza el lambda para calcular la interseccion.
		Punto* intersec = new Punto(rayo->origen->x+lambda*rayo->resta->x, rayo->origen->y+lambda*rayo->resta->y, rayo->origen->z+lambda*rayo->resta->z);
		
		
		// Se chequea que est� adentro del cuadrado.
		bool adentro = true;
		if(X_mayorA_Y(pA->x , pC->x)) {
			if((X_menorA_Y(pA->x , intersec->x)) || (X_mayorA_Y(pC->x , intersec->x))) {
				adentro = false;
			}
		} else if(X_menorA_Y(pA->x , pC->x)) {
			if((X_mayorA_Y(pA->x , intersec->x)) || (X_menorA_Y(pC->x , intersec->x))) {
				adentro = false;
			}
		}
		if(adentro && (X_mayorA_Y(pA->y , pC->y))) {
			if((X_menorA_Y(pA->y , intersec->y)) || (X_mayorA_Y(pC->y , intersec->y))) {
				adentro = false;
			}
		} else if(adentro && (X_menorA_Y(pA->y , pC->y))) {
			if((X_mayorA_Y(pA->y , intersec->y)) || (X_menorA_Y(pC->y , intersec->y))) {
				adentro = false;
			}
		}
		if(adentro && (X_mayorA_Y(pA->z , pC->z))) {
			if((X_menorA_Y(pA->z , intersec->z)) || (X_mayorA_Y(pC->z , intersec->z))) {
				adentro = false;
			}
		} else if(adentro && (X_menorA_Y(pA->z , pC->z))) {
			if((X_mayorA_Y(pA->z , intersec->z)) || (X_menorA_Y(pC->z , intersec->z))) {
				adentro = false;
			}
		}

		if (adentro){
			if (!X_mayorA_Y(((intersec->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
				adentro = false;
			}
		}

		if(adentro) {
			return new Interseccion(intersec, this);
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
}

Vector* Cuadrilatero::normal(Punto* punto) {
	Punto* aux = new Punto(punto);
	aux->sumarPunto(norm->resta);
	return new Vector(punto, aux);
}