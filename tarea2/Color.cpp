#include "Color.h"

Color::Color(){};

Color::~Color(){};

Color::Color(float red, float green, float blue){
	this->red = red;
	this->green = green;
	this->blue = blue;
}

void Color::sumarLuz(float porcentajeLuz, float distanciaLuz, Luz* luz, Objeto* objeto, float prod1, float prod2){

	if (distanciaLuz == 0){
		this->red   = luz->color->red;
		this->green = luz->color->green;
		this->blue  = luz->color->blue;
	} else {
		this->red += porcentajeLuz * ( 1 / distanciaLuz ) * luz->color->red * objeto->getCantLuzDifusa() * prod1 + objeto->getCoefEspecular() * objeto->getColorEspecular()->red * pow((float)abs(prod2), (float)objeto->getExponReflexionEspecular());
		this->green += porcentajeLuz * ( 1 / distanciaLuz ) * luz->color->green * objeto->getCantLuzDifusa() * prod1 + objeto->getCoefEspecular() * objeto->getColorEspecular()->green * pow((float)abs(prod2), (float)objeto->getExponReflexionEspecular());
		this->blue += porcentajeLuz * ( 1 / distanciaLuz ) * luz->color->blue * objeto->getCantLuzDifusa()* prod1 + objeto->getCoefEspecular() * objeto->getColorEspecular()->blue * pow((float)abs(prod2), (float)objeto->getExponReflexionEspecular());

		if (this->red > 1) this->red = 1;
		if (this->green > 1) this->green = 1;
		if (this->blue > 1) this->blue = 1;
	}

};

void Color::mezclarConColor(Color* aux, float coef){
	this->red += aux->red * coef;
	this->green += aux->green * coef;
	this->blue += aux->blue * coef;
	if (this->red > 1) this->red = 1;
	if (this->green > 1) this->green = 1;
	if (this->blue > 1) this->blue = 1;
};

void Color::mezclarConColores(Color* colorIzq, Color* colorDer, Color* colorArriba, Color* colorAbajo, float ponderacion){
	this->red = (this->red * ponderacion + colorIzq->red + colorDer->red + colorArriba->red + colorAbajo->red) / (ponderacion + 4);
	this->green = (this->green * ponderacion + colorIzq->green + colorDer->green + colorArriba->green + colorAbajo->green) / (ponderacion + 4);
	this->blue = (this->blue * ponderacion + colorIzq->blue + colorDer->blue + colorArriba->blue + colorAbajo->blue) / (ponderacion + 4);
};
