#include "FreeImage.h"
#include "Punto.h"
#include "LectorXML.h"
#include "Whitted.h"
#include <math.h>
#include <stdio.h>
#include "SDL.h"

#include <time.h>
#include <iostream>
using namespace std;

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y%m%d-%H%M%S", &tstruct);
    return buf;
}

void DrawPixel(SDL_Surface* Surface, int x, int y, Uint8 R, Uint8 G, Uint8 B){
	Uint32 color = SDL_MapRGB(Surface->format, R, G, B);
	Uint8 * bufp = (Uint8 *)Surface->pixels + y*Surface->pitch + x*Surface->format->BytesPerPixel;
	switch(Surface->format->BytesPerPixel){
	case 4:
		bufp[3] = color >> 24;
	case 3:
		bufp[2] = color >> 16;
	case 2:
		bufp[1] = color >> 8;
	case 1:
		bufp[0] = color;
	}
	return;
}

int main(int argc, char** argv){

	//Inicializo el SDL
	SDL_Surface* screen = NULL;
	if ( SDL_Init(SDL_INIT_VIDEO)<0) {
		std::cout << "Error" << SDL_GetError();
		exit(1);
	}

	atexit(SDL_Quit);



	LectorXML *lector = new LectorXML("pruebaXML.xml");

	Lista* listaObjetos = lector->getListaObjetos();
	Lista* listaLuces = lector->getListaLuces();
	Color* luzAmbiente = lector->getLuzAmbiente();
	DataCamara* datosPantalla = lector->getDataCamara();
	float maxProfundidad = lector->getMaxProfundidad();
	bool verReflexion = lector->getVerReflexion() == 1;
	bool verTransmision = lector->getVerTransmision() == 1;
	int antialiasing = lector->getAntialiasing();
	float pondAnt = lector->getPondAnt();

	float anchoVentana = datosPantalla->anchoVentana;
	float altoVentana  = (anchoVentana * datosPantalla->pixelesY)/datosPantalla->pixelesX;
	float anchoPixel = anchoVentana/datosPantalla->pixelesX;
	float altoPixel  = altoVentana/datosPantalla->pixelesY;

	screen = SDL_SetVideoMode(datosPantalla->pixelesX,datosPantalla->pixelesY,32,SDL_HWSURFACE);
	if (screen == NULL){
		std::cout << "Error poniendo video:" << SDL_GetError();
	}

	DLL_API void DLL_CALLCONV FreeImage_Initialise(BOOL load_local_plugins_only FI_DEFAULT(FALSE));
    FIBITMAP *bitmap = FreeImage_Allocate(datosPantalla->pixelesX,datosPantalla->pixelesY,4 * 8);
    BYTE *bits = (BYTE*)FreeImage_GetBits(bitmap);

	Color* colorPixel;
	Rayo* rayo;

	float dstRayoX =   - anchoVentana/2; 
	float dstRayoY =   -  altoVentana/2;

	for(int y = 0; y < datosPantalla->pixelesY; y++){
		BYTE *pixel = (BYTE*)bits;
		dstRayoX = - anchoVentana/2; 
		for(int x = 0; x < datosPantalla->pixelesX; x++){
			//-1 es porque el origen no es un objeto, sino que es la camara y el id origen empieza en 0 para los objetos.
			rayo = new Rayo(-1,new Vector(new Punto(datosPantalla->x,datosPantalla->y,datosPantalla->z),new Punto(dstRayoX,dstRayoY,datosPantalla->z + datosPantalla->distCamaraVentana)), COEF_REFRAC_AIRE);
			colorPixel = traza_RR(rayo, listaObjetos, listaLuces, 1, maxProfundidad, luzAmbiente, verReflexion, verTransmision);

			if (y == 94 && x == 114){
				cout << "y = 100";
			}

			Uint8 rojo= (_int8) (colorPixel->red * 255.0);
			pixel[FI_RGBA_RED] = rojo;
			Uint8 verde = (_int8) (colorPixel->green * 255.0);
			pixel[FI_RGBA_GREEN] = verde;
			Uint8 azul = (_int8) (colorPixel->blue * 255.0);
			pixel[FI_RGBA_BLUE] = azul;
			pixel[FI_RGBA_ALPHA] = 255;
			pixel += 4;

			DrawPixel(screen,x,datosPantalla->pixelesY - y - 1, rojo, verde, azul);
			

			delete rayo;
			delete colorPixel;

			dstRayoX += anchoPixel;
		}
		SDL_UpdateRect(screen,0,0,datosPantalla->pixelesY,datosPantalla->pixelesY);

		dstRayoY += altoPixel;
		bits+= datosPantalla->pixelesX * 4;
	}

	if (antialiasing){
		Color* colorAct = NULL;
		Color* colorIzq = NULL;
		Color* colorDer = NULL;
		Color* colorAnt = NULL;
		Color* colorPos = NULL;

		BYTE* bitAnt = (BYTE*) FreeImage_GetBits(bitmap);
		BYTE* bitAct = (BYTE*) FreeImage_GetBits(bitmap);
		BYTE* bitPos = (BYTE*) FreeImage_GetBits(bitmap);

		bitAct += datosPantalla->pixelesX * 4;
		bitPos += datosPantalla->pixelesX * 2 * 4;

		for (int y=1; y < datosPantalla->pixelesY - 1; y++){

			BYTE* pixelAnt = (BYTE*) bitAnt;
			BYTE* pixelAct = (BYTE*) bitAct;
			BYTE* pixelPos = (BYTE*) bitPos;

			pixelAnt += 4;
			pixelAct += 4;
			pixelAct += 4;

			for (int x=1; x < datosPantalla->pixelesX - 1; x++){

				colorAct = new Color(pixelAct[FI_RGBA_RED]/255.0,pixelAct[FI_RGBA_GREEN]/255.0,pixelAct[FI_RGBA_BLUE]/255.0);
				pixelAct -= 4;
				colorIzq = new Color(pixelAct[FI_RGBA_RED]/255.0,pixelAct[FI_RGBA_GREEN]/255.0,pixelAct[FI_RGBA_BLUE]/255.0);
				pixelAct += 2 * 4;
				colorDer = new Color(pixelAct[FI_RGBA_RED]/255.0,pixelAct[FI_RGBA_GREEN]/255.0,pixelAct[FI_RGBA_BLUE]/255.0);
				pixelAct -= 4;
				colorAnt = new Color(pixelAnt[FI_RGBA_RED]/255.0,pixelAnt[FI_RGBA_GREEN]/255.0,pixelAnt[FI_RGBA_BLUE]/255.0);
				colorPos = new Color(pixelPos[FI_RGBA_RED]/255.0,pixelPos[FI_RGBA_GREEN]/255.0,pixelPos[FI_RGBA_BLUE]/255.0);
				colorAct->mezclarConColores(colorIzq,colorDer,colorAnt,colorPos,pondAnt);

				_int8 rojo = (_int8) (colorAct->red * 255.0);
				pixelAct[FI_RGBA_RED] = rojo;
				_int8 verde = (_int8) (colorAct->green * 255.0);
				pixelAct[FI_RGBA_GREEN] = verde;
				_int8 azul = (_int8) (colorAct->blue * 255.0);
				pixelAct[FI_RGBA_BLUE] = azul;

				pixelAct[FI_RGBA_ALPHA] = 255;

				delete colorAct;
				delete colorIzq;
				delete colorDer;
				delete colorAnt;
				delete colorPos;

				pixelAnt += 4;
				pixelAct += 4;
				pixelPos += 4;
			}

			bitAnt += datosPantalla->pixelesX * 4;
			bitAct += datosPantalla->pixelesX * 4;
			bitPos += datosPantalla->pixelesY * 4;
		}

	}

	SDL_Delay(3000);
	
	char filename[100];
	strcpy(filename,"D:\\Visual Studio\\historico/");
	strcat(filename, currentDateTime().c_str());
	strcat(filename,".png");
	FreeImage_Save(FIF_PNG,bitmap, filename,0);
	int a;
	cout << "Fin." << endl;
	cin >> a;
	return 0;
}
