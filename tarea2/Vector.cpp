#include "Vector.h"
#include "Punto.h"

Vector::Vector(Punto *fin){
	this->origen  = new Punto(0,0,0);
	this->destino = new Punto(fin);
	this->resta = new Punto(fin);
};

Vector::Vector(Punto* origen, Punto* destino){
	this->origen  = new Punto(origen);
	this->destino = new Punto(destino);
	this->resta = new Punto(destino->x - origen->x, destino->y - origen->y, destino->z - origen->z);
};

Vector::Vector(Vector* copiame){
	this->origen = new Punto(copiame->origen);
	this->destino = new Punto(copiame->destino);
	this->resta = new Punto(copiame->resta);
};

Vector::Vector(Vector* a, Vector* b){
	this->origen = new Punto(a->origen,b->origen);
	this->destino = new Punto(a->destino,b->destino);
	this->resta = new Punto(this->destino->x - this->origen->x, this->destino->y - this->origen->y, this->destino->z - this->origen->z);
};

Vector::~Vector(){
	delete this->origen;
	delete this->destino;
	delete this->resta;
};

void Vector::trasladarACero(){
	this->destino->restarPunto(this->origen);
	this->origen ->restarPunto(this->origen);
	
};

void Vector::normalizar(){
	//Normalizado.
	float modulo = this->modulo();
	if (modulo != 0){
		//this->origen ->dividir(modulo);
		this->resta->dividir(modulo);
		delete this->destino;
		this->destino = new Punto(this->origen->x+this->resta->x, this->origen->y+this->resta->y, this->origen->z+this->resta->z);
	}
};

float Vector::modulo(){
	return sqrt(
			(this->destino->x - this->origen->x)*(this->destino->x - this->origen->x)+
			(this->destino->y - this->origen->y)*(this->destino->y - this->origen->y)+
			(this->destino->z - this->origen->z)*(this->destino->z - this->origen->z)
			);
};

float Vector::productoEscalar(Vector* otro){
	return ((this->resta->x)*(otro->resta->x) +
			(this->resta->y)*(otro->resta->y) +
			(this->resta->z)*(otro->resta->z));
};

Vector* Vector::productoVectorial(Vector *otro) {
	return new Vector(new Punto(this->resta->y * otro->resta->z - this->resta->z * otro->resta->y,
								this->resta->z * otro->resta->x - this->resta->x * otro->resta->z,
								this->resta->x * otro->resta->y - this->resta->y * otro->resta->x));
}

float Vector::angulo(Vector* v) {
	return acos(this->productoEscalar(v) / (this->modulo() * v->modulo()));
}

Vector* Vector::calcularVectorReflexion(Vector* normal){
	Cuaternion* qP = new Cuaternion(0, -this->resta->x, -this->resta->y, -this->resta->z);
	//float theta = -( 3.14158 - this->angulo(normal));
	Vector* normAux = new Vector(normal);
	float theta = this->angulo(normAux);
	if(theta < (3.14159/2)) {
		// El rayo viene de adentro.
		normAux->darVuelta();
	} else {
		// El rayo viene de afuera.
		theta = 3.14159 - theta;
	}
	theta = -2*theta;
	Vector* eje = this->productoVectorial(normAux);
	eje->normalizar();
	Cuaternion* qR = new Cuaternion(cos(theta/2), eje->resta->x*sin(theta/2), eje->resta->y*sin(theta/2), eje->resta->z*sin(theta/2));
	Cuaternion* qRConj = qR->conjudado();
	Cuaternion* qPRotado = qR->multiplicar(qP)->multiplicar(qRConj);
	Vector* res = new Vector(normAux->origen, new Punto(normAux->origen->x + qPRotado->a1, normAux->origen->y + qPRotado->a2, normAux->origen->z + qPRotado->a3));
	delete eje;
	delete qR;
	delete qRConj;
	delete qPRotado;
	delete normAux;
	return res;
};

void Vector::darVuelta() {
	this->resta->darVuelta();
	delete this->destino;
	this->destino = new Punto(this->origen->x + this->resta->x, this->origen->y + this->resta->y, this->origen->z + this->resta->z);
}


Vector* Vector::calcularVectorTransmision(Vector* normal, float indRefOrigen, float indRefDestino) {
	Cuaternion* qP = new Cuaternion(0, this->resta->x, this->resta->y, this->resta->z);
	//float thetaUno = 3.14159 - this->angulo(normal);
	Vector* normAux = new Vector(normal);
	float thetaUno = this->angulo(normAux);
	if(thetaUno < (3.14159/2)) {
		// El rayo viene de adentro.
		normAux->darVuelta();
	} else {
		// El rayo viene de afuera.
		thetaUno = 3.14159 - thetaUno;
	}
	float thetaDos = asin(indRefOrigen*sin(thetaUno)/indRefDestino);
	float theta = -(thetaUno-thetaDos);
	Vector* eje = this->productoVectorial(normAux);
	eje->normalizar();
	Cuaternion* qR = new Cuaternion(cos(theta/2), eje->resta->x*sin(theta/2), eje->resta->y*sin(theta/2), eje->resta->z*sin(theta/2));
	Cuaternion* qRConj = qR->conjudado();
	Cuaternion* qPRotado = qR->multiplicar(qP)->multiplicar(qRConj);
	Vector* res = new Vector(normAux->origen, new Punto(normAux->origen->x + qPRotado->a1, normAux->origen->y + qPRotado->a2, normAux->origen->z + qPRotado->a3));
	delete eje;
	delete qR;
	delete qRConj;
	delete qPRotado;
	delete normAux;
	return res;
}