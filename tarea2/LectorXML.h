#ifndef LECTORXML_H
#define LECTORXML_H

#include "Cilindro.h"
#include "Esfera.h"
#include "Cuadrilatero.h"
#include "Triangulo.h"
#include "DataCamara.h"
#include "Lista.h"

#include "pugixml\pugixml.hpp"

class LectorXML {
	private:
		pugi::xml_document datos;
	public :
		LectorXML(char *path);
		//~LectorXML();
		Lista * getListaObjetos();
		Lista * getListaLuces();
		Color * getLuzAmbiente();	// return [float r, float g, float b]
		DataCamara * getDataCamara();
		float getMaxProfundidad();
		int getBlackAndWhite();
		int getAntialiasing();
		float getPondAnt();
		int getVerReflexion();
		int getVerTransmision();
};

#endif