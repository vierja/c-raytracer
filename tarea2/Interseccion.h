#ifndef INTERSECCION_H
#define	INTERSECCION_H
class Objeto;
class Punto;
class Vector;

class Interseccion{
	private:
	public:
		Punto* punto;

		Objeto* objeto;
		Interseccion(Punto* p, Objeto* o);
		~Interseccion();
		Interseccion* copiarInterseccion(Interseccion* i);
		Vector* calcularNormal();
};

#endif
