#include "DataCamara.h"

DataCamara::DataCamara(float x, float y, float z, float distCamaraVentana, int pixelesX, int pixelesY, float anchoVentana) {
	
	this->x = x;
	this->y = y;
	this->z = z;
	this->distCamaraVentana = distCamaraVentana;
	this->pixelesX = pixelesX;
	this->pixelesY = pixelesY;
	this->anchoVentana = anchoVentana;
}