#include "Rayo.h"

Rayo::Rayo(int idObjetoOrigen,Vector* ray, float indRefraccion) {
	this->idObjetoOrigen = idObjetoOrigen;
	this->ray = ray;
	this->indRefraccion = indRefraccion;
};

Rayo::~Rayo(){
	delete ray;
};

Vector* Rayo::calcularVectorTransmision(Vector* normal, float indRefDestino) {
	return ray->calcularVectorTransmision(normal, indRefraccion, indRefDestino);
}