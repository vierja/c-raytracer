#include "Whitted.h"
#include "Punto.h"
#include "Color.h"
#include <stdio.h>

#include <time.h>
#include <iostream>
using namespace std;

Color* traza_RR(Rayo* rayo, Lista* listaObjetos, Lista* listaLuces, int profundidad, int maxProfundidad, Color* luzAmbiental, bool verReflexion, bool verTransmision){
	/*
		Pseudo codigo:
		determinar la intereseccion mas cercana de un rayo con un objeto;
		if (hay objeto intesercado){

			calcular la normal en la inteseccion
			return sombra_RR(Objeto *objeto, Vector *rayo, Punto *punto, Vector *normal, int profundidad, int maxProfundidad, Lista *listaObjetos, Lista *luces, Color *luzAmbiental)
		} else {

			return new Color(0,0,0);
		}
	*/

	Lista* listaIntersecciones = new Lista();
	Interseccion* interseccionRayo;
	Lista* auxListaObjetos = listaObjetos;
	Objeto* obj = NULL;

	while (!auxListaObjetos->isVacia()){
		obj = (Objeto*) auxListaObjetos->getElemento();
		interseccionRayo = obj ->intersectaRayo(rayo);
		if (interseccionRayo!=NULL && rayo->ray->origen->distinto(interseccionRayo->punto)){
			listaIntersecciones->add(interseccionRayo);
		}

		auxListaObjetos = auxListaObjetos->getNext();
	}

	//Ahora recorro la lista buscando cual es el punto y objeto

	Lista* auxListaIntersecciones = listaIntersecciones;
	Interseccion* interseccionAux;
	interseccionRayo = NULL;

	while (!auxListaIntersecciones->isVacia()){
		interseccionAux = (Interseccion*) auxListaIntersecciones->getElemento();
		if (interseccionRayo == NULL){
			interseccionRayo = interseccionAux;
		} else {
			if (interseccionAux->punto->masCercaDelOrigen(rayo->ray,interseccionRayo->punto)){
				interseccionRayo = interseccionAux;
			}
		}
		auxListaIntersecciones = auxListaIntersecciones->getNext();
	}

	if (interseccionRayo == NULL){
		return new Color(0,0,0);
	}

	Objeto* objet = (Objeto*) interseccionRayo->objeto;
	if (verReflexion){
		return new Color(objet->getCoefEspecular(),objet->getCoefEspecular(),objet->getCoefEspecular());
	} else if (verTransmision){
		return new Color(objet->getCoefTransmision(),objet->getCoefTransmision(),objet->getCoefTransmision());
	}
	Vector* normal = interseccionRayo->calcularNormal();
	Color* colorPixel;
	colorPixel = sombra_RR(objet, rayo, interseccionRayo->punto, normal, profundidad, maxProfundidad, listaObjetos, listaLuces, luzAmbiental);
	delete listaIntersecciones;
//	delete interseccionRayo;
	delete normal;
	return colorPixel;
};



Color* sombra_RR(Objeto* objeto, Rayo* rayo, Punto* punto, Vector* normal, int profundidad, int maxProfundidad, Lista* listaObjetos, Lista* luces, Color* luzAmbiental){
	//DEBUG
	if (objeto->getCoefEspecular() == 1.0){
		normal = normal;
	}
	Color* colorPixel = new Color();
	//Defino el color inicial. Termino del ambiente
	colorPixel->red   = luzAmbiental->red   * objeto->getCantLuzAmbiental() * objeto->getColor()->red;
	colorPixel->green = luzAmbiental->green * objeto->getCantLuzAmbiental() * objeto->getColor()->green;
	colorPixel->blue  = luzAmbiental->blue  * objeto->getCantLuzAmbiental() * objeto->getColor()->blue;
	//PSEUDO: color = termino del ambiente

	Lista * listaLuces = luces;
	Luz* luzActual;
	Vector* rayoPuntoLuz;
	double porcentajeLuz;
	Rayo* rayoR = NULL;

	if (objeto->getCantLuzDifusa() > 0 || objeto->getCoefEspecular() > 0){
		rayoR = new Rayo(objeto->getId(),rayo->ray->calcularVectorReflexion(normal), rayo->indRefraccion);

		Vector* auxRayoR = new Vector(rayoR->ray);
		Vector* auxRayo  = new Vector(rayo->ray);
		auxRayo->trasladarACero();
		auxRayo->normalizar();
		auxRayoR->trasladarACero();
		auxRayoR->normalizar();

		//PSEUDO: for(cada luz){
		while (!listaLuces->isVacia()){
			//Info basica: Pag. 11
			luzActual = (Luz*) listaLuces->getElemento();
			//Chequeamos si el objeto esta en sombra o no.
			//PSEUDO: rayo_s = rayo desde punto a la luz
			rayoPuntoLuz = new Vector(new Punto(luzActual->centro), new Punto(punto));

			//PSEUDO: if (el producto punto de normal y direccion de luz es positivo){
			if (rayoPuntoLuz->productoEscalar(normal) >= 0){
				//Si el producto escalar es mayor= a cero entonces la fuente de luz esta delante de el objeto.

				//PSEUDO: 	Calcular cuanta luz es bloqueada por superficies opacas y transparantes
				//			y usarlo para escalar los terminos difusos y especulares antes de añadirlos a color;
				Rayo* rayado = new Rayo(objeto->getId(),new Vector(rayoPuntoLuz),objeto->getIndiceRefraccion()/*NOIMPORTA*/);
				porcentajeLuz = calcularPorcentajeLuz(rayado,listaObjetos);
				double distanciaLuz = rayoPuntoLuz->modulo();
				rayoPuntoLuz->trasladarACero();
				rayoPuntoLuz->normalizar();
				colorPixel->sumarLuz(porcentajeLuz,distanciaLuz,luzActual,objeto,rayoPuntoLuz->productoEscalar(normal), auxRayoR->productoEscalar(auxRayo));
				delete rayado;
			}
			listaLuces = listaLuces->getNext();

			delete rayoPuntoLuz;
		}

		delete auxRayo;
		delete auxRayoR;
	}

	Color* colorAux = NULL;

	//PSEUDO: if (profundidad < profundidad_max){
	if (profundidad < maxProfundidad){
		//PSEUDO: if (objeto es reflejante) {
		if (objeto->getCoefEspecular() > 0){
			//El objeto refleja.
			//PSEUDO: rayo_r = rayo en direccion de reflexion desde punto;
			//PSEUDO: color_r = traza_RR(rayo_r,profundidad+1);
			colorAux = traza_RR(rayoR,listaObjetos,listaLuces,profundidad+1,maxProfundidad,luzAmbiental,false,false);

			//Escalar color_r por el coeficiente especular y anadir a color
			if (colorAux != NULL){
				colorPixel->mezclarConColor(colorAux, objeto->getCoefEspecular());
			}
		}
		//PSEUDO: if (objeto es transparente){
		if (objeto->getCoefTransmision() > 0 && objeto->getIndiceRefraccion() ){
			//El objeto es algo transparente.
			//PSEUDO: if (no ocurre la reflexion interna total){ //TODO
			//PSEUDO: rayo_t = rayo en la direccion de refraccion desde punto;

			Rayo* rayoT = new Rayo(objeto->getId(), rayo->calcularVectorTransmision(normal,objeto->getIndiceRefraccion()), objeto->getIndiceRefraccion());
			//PSEUDO: color_t = traza_RR(rayo_t, profundidad+1);
			colorAux = traza_RR(rayoT,listaObjetos,listaLuces,profundidad+1,maxProfundidad,luzAmbiental,false,false);
			//PSEUDO: Escalar color_t por el coeficiente de transmission y anadir a color
			if (colorAux != NULL){
				colorPixel->mezclarConColor(colorAux, objeto->getCoefTransmision());
			}

			delete rayoT;
		}
	}

	delete rayoR;


	return colorPixel;

};

double calcularPorcentajeLuz(Rayo* rayo, Lista* listaObjetos){
	Vector* rayoPuntoLuz = rayo->ray;
	double porcentajeLuz = 1;
	Interseccion* intersecta;
	Objeto* obj;
	while (!listaObjetos->isVacia()){
		obj = (Objeto*) listaObjetos->getElemento();
		intersecta = obj->intersectaRayo(rayo);
		if (intersecta != NULL && (intersecta->punto->entreOrigenYDestino(rayoPuntoLuz))){
			porcentajeLuz *= ((Objeto*) listaObjetos->getElemento())->getCoefTransmision();
		}
		delete intersecta;
		listaObjetos = listaObjetos->getNext();
	}
	return porcentajeLuz;
};
