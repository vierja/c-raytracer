#ifndef LUZ_H
#define LUZ_H

class Color;
class Punto;

class Luz {
public:
	Punto* centro;
	Color* color;

	Luz(Punto* centro, Color* color);
	~Luz();
};

#endif
