#include "Definiciones.h"
#include <math.h>

bool X_menorA_Y(float x, float y){
	return y - x > MINIMO_ERROR;
	return x + MINIMO_ERROR < y;
};

bool X_mayorA_Y(float x, float y){
	return x - y > MINIMO_ERROR;
	return x > y + MINIMO_ERROR;
};

bool X_igualA_Y(float x, float y){
	return abs(x - y) < MINIMO_ERROR;
};