#ifndef LISTA_H
#define LISTA_H

#include <cstdlib>

class Lista {
	private:
		void *elemento;
		Lista *sig;
	public:
		Lista();
		~Lista();

		void add(void * elemento);

		void * getElemento();
		Lista * getNext();
		bool isVacia();
};

#endif