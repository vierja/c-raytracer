#include "Interseccion.h"
#include "Punto.h"
#include "Objeto.h"
#include "Vector.h"
#include <stdlib.h>


Interseccion::Interseccion(Punto* p, Objeto* o){
	this->punto = p;
	this->objeto = o;
}

Interseccion::~Interseccion(){
    delete this->punto;
    //No se borra el objeto
}
Interseccion * Interseccion::copiarInterseccion(Interseccion* i){
	return new Interseccion(i->punto,i->objeto);
}

Vector* Interseccion::calcularNormal(){
	return this->objeto->normal(this->punto);
}