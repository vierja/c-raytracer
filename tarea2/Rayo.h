#ifndef RAYO_H
#define RAYO_H

#include "Vector.h"
class Rayo {
public:
	Vector* ray;
	float indRefraccion;
	int idObjetoOrigen;

	Rayo(int idObjetoOrigen, Vector* ray, float indRefraccion);
	~Rayo();

	Vector* calcularVectorTransmision(Vector* normal, float indRefDestino);
};

#endif