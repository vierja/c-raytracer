#include "LectorXML.h"

#include <iostream>
using namespace std;

LectorXML::LectorXML(char* path) {
	if (!datos.load_file(path)){
        cout << "Error al cargar archivo xml.\n" << endl;	// Hacerlo afuera
    }
}

Lista * LectorXML::getListaObjetos() {
	Lista * list = new Lista();
	int contador = 0;
	pugi::xml_node objetos = datos.child("escena").child("objetos");
    for (pugi::xml_node objeto = objetos.child("esfera"); objeto; objeto = objeto.next_sibling("esfera")){
		Esfera *dat = new Esfera(contador,new Punto(atof(objeto.attribute("xCentro").value()), atof(objeto.attribute("yCentro").value()), atof(objeto.attribute("zCentro").value())),
								atof(objeto.attribute("radio").value()),
								new Color(atof(objeto.attribute("r").value()), atof(objeto.attribute("g").value()), atof(objeto.attribute("b").value())),
								new Color(atof(objeto.attribute("rEsp").value()), atof(objeto.attribute("gEsp").value()), atof(objeto.attribute("bEsp").value())),
								atof(objeto.attribute("luzAmbiente").value()), atof(objeto.attribute("luzDifusa").value()),
								atof(objeto.attribute("coefEspecular").value()), atof(objeto.attribute("expRefEspecular").value()),
								atof(objeto.attribute("coefTrans").value()), atof(objeto.attribute("indRefraccion").value()));
		list->add(dat);
		contador++;
    }

	for (pugi::xml_node objeto = objetos.child("cilindro"); objeto; objeto = objeto.next_sibling("cilindro")){
		Cilindro *dat = new Cilindro(contador,new Punto(atof(objeto.attribute("xCentroBase").value()), atof(objeto.attribute("yCentroBase").value()), atof(objeto.attribute("zCentroBase").value())),
										atof(objeto.attribute("radio").value()), atof(objeto.attribute("altura").value()),
										new Color(atof(objeto.attribute("r").value()), atof(objeto.attribute("g").value()), atof(objeto.attribute("b").value())),
										new Color(atof(objeto.attribute("rEsp").value()), atof(objeto.attribute("gEsp").value()), atof(objeto.attribute("bEsp").value())),
										atof(objeto.attribute("luzAmbiente").value()), atof(objeto.attribute("luzDifusa").value()),
										atof(objeto.attribute("coefEspecular").value()), atof(objeto.attribute("expRefEspecular").value()),
										atof(objeto.attribute("coefTrans").value()), atof(objeto.attribute("indRefraccion").value()));
		list->add(dat);
		contador++;
    }

	for (pugi::xml_node objeto = objetos.child("plano"); objeto; objeto = objeto.next_sibling("plano")){
		Cuadrilatero *dat = new Cuadrilatero(contador,new Punto(atof(objeto.attribute("xA").value()), atof(objeto.attribute("yA").value()), atof(objeto.attribute("zA").value())),
										new Punto(atof(objeto.attribute("xB").value()), atof(objeto.attribute("yB").value()), atof(objeto.attribute("zB").value())),
										new Punto(atof(objeto.attribute("xC").value()), atof(objeto.attribute("yC").value()), atof(objeto.attribute("zC").value())),
										new Punto(atof(objeto.attribute("xD").value()), atof(objeto.attribute("yD").value()), atof(objeto.attribute("zD").value())),
										new Color(atof(objeto.attribute("r").value()), atof(objeto.attribute("g").value()), atof(objeto.attribute("b").value())),
										new Color(atof(objeto.attribute("rEsp").value()), atof(objeto.attribute("gEsp").value()), atof(objeto.attribute("bEsp").value())),
										atof(objeto.attribute("luzAmbiente").value()), atof(objeto.attribute("luzDifusa").value()),
										atof(objeto.attribute("coefEspecular").value()), atof(objeto.attribute("expRefEspecular").value()),
										atof(objeto.attribute("coefTrans").value()), atof(objeto.attribute("indRefraccion").value()));
		list->add(dat);
		contador++;
    }

	for (pugi::xml_node objeto = objetos.child("triangulo"); objeto; objeto = objeto.next_sibling("triangulo")){
		Triangulo *dat = new Triangulo(contador,new Punto(atof(objeto.attribute("x1").value()), atof(objeto.attribute("y1").value()), atof(objeto.attribute("z1").value())),
										new Punto(atof(objeto.attribute("x2").value()), atof(objeto.attribute("y2").value()), atof(objeto.attribute("z2").value())),
										new Punto(atof(objeto.attribute("x3").value()), atof(objeto.attribute("y3").value()), atof(objeto.attribute("z3").value())),
										new Color(atof(objeto.attribute("r").value()), atof(objeto.attribute("g").value()),atof(objeto.attribute("b").value())), 
										new Color(atof(objeto.attribute("rEsp").value()), atof(objeto.attribute("gEsp").value()), atof(objeto.attribute("bEsp").value())),
										atof(objeto.attribute("luzAmbiente").value()),
										atof(objeto.attribute("luzDifusa").value()), atof(objeto.attribute("coefEspecular").value()),
										atof(objeto.attribute("expRefEspecular").value()), atof(objeto.attribute("coefTrans").value()),
										atof(objeto.attribute("indRefraccion").value()));
		list->add(dat);
		contador++;
    }
	return list;
}

Lista * LectorXML::getListaLuces() {
	Lista * list = new Lista();
	pugi::xml_node objetos = datos.child("escena").child("luces");

	for (pugi::xml_node objeto = objetos.child("luz"); objeto; objeto = objeto.next_sibling("luz")){
		Luz *dat = new Luz(new Punto(atof(objeto.attribute("x").value()), atof(objeto.attribute("y").value()), atof(objeto.attribute("z").value())),
							new Color(atof(objeto.attribute("r").value()), atof(objeto.attribute("g").value()), atof(objeto.attribute("b").value())));
		list->add(dat);
    }
	return list;
}

Color * LectorXML::getLuzAmbiente() {
	float * luzAmb = new float[3];
	pugi::xml_node objetos = datos.child("escena").child("luces");

	return new Color(atof(objetos.child("luzAmbiente").attribute("r").value()),
					atof(objetos.child("luzAmbiente").attribute("g").value()),
					atof(objetos.child("luzAmbiente").attribute("b").value()));
}

DataCamara * LectorXML::getDataCamara() {
	pugi::xml_node objeto = datos.child("escena").child("camara");
	return new DataCamara(atof(objeto.attribute("x").value()), atof(objeto.attribute("y").value()), atof(objeto.attribute("z").value()),
										atof(objeto.attribute("distCamaraVentana").value()), atoi(objeto.attribute("pixelesX").value()),
										atoi(objeto.attribute("pixelesY").value()), atof(objeto.attribute("anchoVentana").value()));
}

float LectorXML::getMaxProfundidad() {
	return atof(datos.child("escena").child("otros").attribute("maxProfundidad").value());
}

int LectorXML::getBlackAndWhite() {
	return atof(datos.child("escena").child("otros").attribute("blackAndWhite").value());
}

int LectorXML::getAntialiasing() {
	return atof(datos.child("escena").child("otros").attribute("antialiasing").value());
}

float LectorXML::getPondAnt() {
	return atof(datos.child("escena").child("otros").attribute("pondAnt").value());
}

int LectorXML::getVerReflexion() {
	return atof(datos.child("escena").child("otros").attribute("verReflexion").value());
}

int LectorXML::getVerTransmision() {
	return atof(datos.child("escena").child("otros").attribute("verTransmision").value());
}
