#ifndef OBJETO_H
#define OBJETO_H

#include "Definiciones.h"
#include "Rayo.h"
#include "Color.h"
#include "Interseccion.h"

class Objeto {
public:
	virtual int getId() = 0;
	virtual TipoObjeto getTipoObjeto() = 0;
	virtual Color* getColor() = 0;
	virtual Color* getColorEspecular() = 0;
	virtual float getCantLuzAmbiental() = 0;
	virtual float getCantLuzDifusa() = 0;
	virtual float getCoefEspecular() = 0;
	virtual float getExponReflexionEspecular() = 0;
	virtual float getCoefTransmision() = 0;
	virtual float getIndiceRefraccion() = 0;
	virtual Interseccion* intersectaRayo(Rayo* rayo) = 0; 
	//TODO: Agregar en todas las subclases de Objeto e implementar
	virtual Vector* normal (Punto* punto) = 0;
};

#endif