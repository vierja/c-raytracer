#ifndef PUNTO_H
#define PUNTO_H

#include <math.h>

class Vector;

class Punto {
	private:
	public:
		double x;
		double y;
		double z;
		Punto(double x, double y, double z);
		Punto(Punto* copiame);
		Punto(Punto* a, Punto* b);
		~Punto();
		double distancia(Punto* p);
		bool igual(Punto* otro);
		bool distinto(Punto* otro);
		void sumarPunto(Punto* tevoyasumar);
		void restarPunto(Punto* tevoyarrestar);
		void dividir(double tevoyadividirputo);
		void multiplicar(double tevoyamultiplicar);
		void darVuelta();
		bool masCercaDelOrigen(Vector* rayo, Punto* punto);
		bool Punto::entreOrigenYDestino(Vector* vector);
};

#endif