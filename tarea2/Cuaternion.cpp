#include "Cuaternion.h"

Cuaternion::Cuaternion(float a, float a1, float a2, float a3) {
	this->a = a;
	this->a1 = a1;
	this->a2 = a2;
	this->a3 = a3;
}

Cuaternion::~Cuaternion(){
};
Cuaternion* Cuaternion::conjudado() {
	return new Cuaternion(a, -a1, -a2, -a3);
}

Cuaternion* Cuaternion::multiplicar(Cuaternion* cuat) {
	float b = cuat->a;
	float b1 = cuat->a1;
	float b2 = cuat->a2;
	float b3 = cuat->a3;
	return new Cuaternion(a*b-a1*b1-a2*b2-a3*b3,
							a*b1+b*a1+a2*b3-a3*b2,
							b*a2+a*b2+a3*b1-a1*b3,
							b*a3+a*b3-a2*b1+a1*b2);
}