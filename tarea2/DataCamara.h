#ifndef DATACAMARA_H
#define DATACAMARA_H

class DataCamara {
	private:
	public:
		float x;
		float y;
		float z;
		float distCamaraVentana;
		int pixelesX;
		int pixelesY;
		float anchoVentana;

		DataCamara(float x, float y, float z, float distCamaraVentana, int pixelesX, int pixelesY, float anchoVentana);
};

#endif