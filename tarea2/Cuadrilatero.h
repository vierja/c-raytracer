#ifndef CUADRILATERO_H
#define CUADRILATERO_H

#include "Punto.h"
#include "Color.h"
#include "Objeto.h"
#include "Definiciones.h"

#include "stdlib.h"

class Cuadrilatero: public Objeto {
public:
	Punto* pA;
	Punto* pB;
	Punto* pC;
	Punto* pD;
	Vector *norm;
	int id;
	//float ancho;
	//float alto;
	//int planoBase;
	Color* color;
	Color* colorEspecular;

	float cantLuzAmbiental;
	float cantLuzDifusa; 
	float coefEspecular;
	int	   exponReflexionEspecular;
	float coefTransmision;
	float indiceRefraccion;

	Cuadrilatero(int id,Punto* pA, Punto* pB, Punto* pC, Punto* pD, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, int exponReflexionEspecular, float coefTransmision, float indiceRefraccion);

	TipoObjeto getTipoObjeto();
	int getId();
	Color* getColor();
	Color* getColorEspecular();
	float getCantLuzAmbiental();
	float getCantLuzDifusa();
	float getCoefEspecular();
	float getExponReflexionEspecular();
	float getCoefTransmision();
	float getIndiceRefraccion();
	Interseccion* intersectaRayo(Rayo* rayoR);
	Vector* normal(Punto* punto);
};

#endif