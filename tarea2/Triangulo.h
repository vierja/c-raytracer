#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "Punto.h"
#include "Color.h"
#include "Objeto.h"

class Triangulo: public Objeto {
public:
	Punto* p1;
	Punto* p2;
	Punto* p3;
	int id;
	// Para chequear si punto pertenece al triangulo.
	Vector* v0;
	Vector* v1;
	Vector* vectorialV0V0;
	Vector* vectorialV0V1;
	Vector* vectorialV1V1;

	Vector* norm;

	Color* color;
	Color* colorEspecular;

	float cantLuzAmbiental;
	float cantLuzDifusa;
	float coefEspecular;
	float exponReflexionEspecular;
	float coefTransmision;
	float indiceRefraccion;

	Triangulo(int id, Punto* p1, Punto* p2, Punto* p3, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion);
	
	TipoObjeto getTipoObjeto();
	int getId();
	Color* getColor();
	Color* getColorEspecular();
	float getCantLuzAmbiental();
	float getCantLuzDifusa();
	float getCoefEspecular();
	float getExponReflexionEspecular();
	float getCoefTransmision();
	float getIndiceRefraccion();
	Vector* normal(Punto* punto);
	Interseccion* intersectaRayo(Rayo* rayoR); 
};

#endif