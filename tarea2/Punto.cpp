#include "Punto.h"
#include "Vector.h"
#include "Definiciones.h"

Punto::Punto(double x, double y, double z){
	this->x = x;
	this->y = y;
	this->z = z;
}

Punto::Punto(Punto* copiame){
	this->x = copiame->x;
	this->y = copiame->y;
	this->z = copiame->z;
};
Punto::Punto(Punto* a, Punto* b){
	this->x = a->x + b->x;
	this->y = a->y + b->y;
	this->z = a->z + b->z;
};

Punto::~Punto(){

};
double cuadrado(double numero){
    return numero*numero;
};

bool Punto::distinto(Punto* otro){
	return !this->igual(otro);
};

bool Punto::igual(Punto* otro){
	return X_igualA_Y(this->x, otro->x) && X_igualA_Y(this->y, otro->y) && X_igualA_Y(this->z, otro->z);
};

double Punto::distancia(Punto* p){
	return sqrt(cuadrado(this->x-p->x)+cuadrado(this->y-p->y)+cuadrado(this->z-p->z));
}

void Punto::sumarPunto(Punto* tevoyasumar){
	this->x += tevoyasumar->x;
	this->y += tevoyasumar->y;
	this->z += tevoyasumar->z;
};

void Punto::restarPunto(Punto* tevoyarrestar){
	this->x -= tevoyarrestar->x;
	this->y -= tevoyarrestar->y;
	this->z -= tevoyarrestar->z;

};

void Punto::dividir(double tevoyadividirputo){
	this->x /= tevoyadividirputo;
	this->y /= tevoyadividirputo;
	this->z /= tevoyadividirputo;
};

void Punto::multiplicar(double tevoyamultiplicar){
	this->x *= tevoyamultiplicar;
	this->y *= tevoyamultiplicar;
	this->z *= tevoyamultiplicar;
}

void Punto::darVuelta() {
	x = -x;
	y = -y;
	z = -z;
}

bool Punto::masCercaDelOrigen(Vector* rayo, Punto* punto){
	/*
	Se crea dos vectores.
	Uno desde el origen del rayo al punto 'punto'
	Y otro desde el origen del rayo al punto donde estamos.
	Si el modulo del primero es mas grande que del segundo entonces
	el segundo se encuentra mas cerca del origen.
	*/
	Vector* origenPunto = new Vector(new Punto(rayo->origen), new Punto(punto));
	Vector* origenThis  = new Vector(new Punto(rayo->origen), new Punto(this));
	bool masLargo = (origenPunto->modulo() > origenThis->modulo());
	delete origenPunto;
	delete origenThis;
	return masLargo;
};

bool Punto::entreOrigenYDestino(Vector* vector){

	if ((X_igualA_Y(this->x , vector->origen->x) && X_igualA_Y(this->y , vector->origen->y) && X_igualA_Y(this->z , vector->origen->z)) || (X_igualA_Y(this->x  , vector->destino->x) && X_igualA_Y(this->y , vector->destino->y) && X_igualA_Y(this->z , vector->destino->z))){
		return false;
	}
	if(X_mayorA_Y(vector->destino->x , vector->origen->x)) {
		if((X_mayorA_Y(vector->origen->x , this->x)) || (X_menorA_Y(vector->destino->x , this->x))) {
			return false;
		}
	} else if (X_menorA_Y(vector->destino->x , vector->origen->x)) {
		if((X_menorA_Y(vector->origen->x , this->x)) || (X_mayorA_Y(vector->destino->x , this->x))) {
			return false;
		}
	} else {
		if (X_menorA_Y(vector->destino->x , this->x)){
			return false;
		}
	}
	if(X_mayorA_Y(vector->destino->y , vector->origen->y)) {
		if((X_mayorA_Y(vector->origen->y , this->y)) || (X_menorA_Y(vector->destino->y , this->y))) {
			return false;
		}
	} else if (X_menorA_Y(vector->destino->y , vector->origen->y)) {
		if((X_menorA_Y(vector->origen->y , this->y)) || (X_mayorA_Y(vector->destino->y , this->y))) {
			return false;
		}
	} else {
		if (X_menorA_Y(vector->destino->y , this->y)){
			return false;
		}
	}
	if(X_mayorA_Y(vector->destino->z , vector->origen->z)) {
		if((X_mayorA_Y(vector->origen->z , this->z)) || (X_menorA_Y(vector->destino->z , this->z))) {
			return false;
		}
	} else if (X_menorA_Y(vector->destino->z , vector->origen->z)) {
		if((X_menorA_Y(vector->origen->z , this->z)) || (X_mayorA_Y(vector->destino->z , this->z))) {
			return false;
		}
	} else {
		if (X_menorA_Y(vector->destino->z , this->z)){
			return false;
		}
	}
	return true;
};
