#ifndef ESFERA_H
#define ESFERA_H

#include "Punto.h"
#include "Color.h"
#include "Objeto.h"
#include "Definiciones.h"
#include "Rayo.h"
#include "Math.h"

class Esfera: public Objeto {
public:
	Punto* centro;
	float radio;
	int id;
	Color* color;
	Color* colorEspecular;

	float cantLuzAmbiental;
	float cantLuzDifusa;
	float coefEspecular;
	float exponReflexionEspecular;
	float coefTransmision;
	float indiceRefraccion;

	Esfera(int id,Punto* centro, float radio, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion);

	TipoObjeto getTipoObjeto();
	int getId();
	Color* getColor();
	Color* getColorEspecular();
	float getCantLuzAmbiental();
	float getCantLuzDifusa();
	float getCoefEspecular();
	float getExponReflexionEspecular();
	float getCoefTransmision();
	float getIndiceRefraccion();
	Interseccion* intersectaRayo(Rayo* rayo); 
	Vector* normal (Punto* punto);

};

#endif