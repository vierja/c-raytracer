#ifndef DEFINICIONES_H
#define DEFINICIONES_H

//enum TipoObjeto { o_esfera, o_cilindro, o_prisma, o_cuadrilatero, o_triangulo};
enum TipoObjeto {ESFERA, CILINDRO, PRISMA, PLANO, TRIANGULO, CUADRILATERO};
#define MINIMO_ERROR 0.001

bool X_menorA_Y(float x, float y);
bool X_mayorA_Y(float x, float y);
bool X_igualA_Y(float x, float y);

// Refraccion del aire
#define COEF_REFRAC_AIRE 1.00029


#endif