#ifndef COLOR_H
#define COLOR_H

#include "Luz.h"
#include "Objeto.h"
#include <math.h>

class Color {
	private:
	public :
		float red;
		float green;
		float blue;

		Color();
		~Color();
		Color(float red, float green, float blue);
		void sumarLuz(float porcentajeLuz, float distanciaLuz, Luz* luz, Objeto* objeto, float prod1, float prod2);
		void mezclarConColor(Color* aux, float coef);
		void mezclarConColores(Color* colorIzq, Color* colorDer, Color* colorArriba, Color* abajo, float ponderacion);
};

#endif
