#ifndef CUATERNION_H
#define CUATERNION_H

#include "Punto.h"

class Cuaternion {
	private:
	public :
		float a;
		float a1;
		float a2;
		float a3;

		Cuaternion(float a, float a1, float a2, float a3);
		~Cuaternion();
		Cuaternion* conjudado();
		Cuaternion* multiplicar(Cuaternion* cuat);
};

#endif
