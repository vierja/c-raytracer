#include "Lista.h"

Lista::Lista() {
	elemento = NULL;
	sig = NULL;
}

Lista::~Lista(){
	delete this->elemento;
	delete sig;
}
void Lista::add(void * elemento) {
	Lista *aux = this;
	while(aux->elemento != NULL) {
		aux = aux->sig;
	}
	aux->elemento = elemento;
	aux->sig = new Lista();
}

void * Lista::getElemento() {
	return elemento;
}

Lista * Lista::getNext() {
	return sig;
}

bool Lista::isVacia() {
	return (elemento == NULL);
}