#include "Triangulo.h"
#include <stdlib.h>

Triangulo::Triangulo(int id,Punto* p1, Punto* p2, Punto* p3, Color* color, Color* colorEspecular, float cantLuzAmbiental, float cantLuzDifusa, float coefEspecular, float exponReflexionEspecular, float coefTransmision, float indiceRefraccion){
	this->id = id;
	this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
	/*this->v0 = new Vector(p1,p2);
	this->v1 = new Vector(p1,p3);
	this->vectorialV0V0 = v0->productoVectorial(v0);
	this->vectorialV0V1 = v0->productoVectorial(v1);
	this->vectorialV1V1 = v1->productoVectorial(v1);*/
	Vector *BA = new Vector(p1, p2);
	Vector *DA = new Vector(p1, p3);
	this->norm = BA->productoVectorial(DA);
	this->color = color;
	this->colorEspecular = colorEspecular;
	this->cantLuzAmbiental = cantLuzAmbiental;
	this->cantLuzDifusa = cantLuzDifusa;
	this->coefEspecular = coefEspecular;
	this->exponReflexionEspecular = exponReflexionEspecular;
	this->coefTransmision = coefTransmision;
	this->indiceRefraccion = indiceRefraccion;
};

int Triangulo::getId(){ return id; };

TipoObjeto Triangulo::getTipoObjeto(){
	return TRIANGULO;
};

Color* Triangulo::getColor(){
	return this->color;
};
Color* Triangulo::getColorEspecular(){
	return this->colorEspecular;
};
float Triangulo::getCantLuzAmbiental(){
	return this->cantLuzAmbiental;
};
float Triangulo::getCantLuzDifusa(){
	return this->cantLuzDifusa;
};
float Triangulo::getCoefEspecular(){
	return this->coefEspecular;
};
float Triangulo::getExponReflexionEspecular(){
	return this->exponReflexionEspecular;
};
float Triangulo::getCoefTransmision(){
	return this->coefTransmision;
};
float Triangulo::getIndiceRefraccion(){
	return this->indiceRefraccion;
};
Vector* Triangulo::normal(Punto* punto){
	Punto* aux = new Punto(punto);
	aux->sumarPunto(norm->resta);
	return new Vector(punto, aux);
}

bool mismoLado(Punto* p1, Punto* p2, Punto* a, Punto* b) {
	Vector* ba = new Vector(a, b);
	Vector* p1a = new Vector(a, p1);
	Vector* p2a = new Vector(a, p2);

	Vector* cp1 = ba->productoVectorial(p1a);
	Vector* cp2 = ba->productoVectorial(p2a);

	if((cp1->productoEscalar(cp2)) >= 0) {
		delete ba;
		delete p1a;
		delete p2a;
		delete cp1;
		delete cp2;
		return true;
	} else {
		delete ba;
		delete p1a;
		delete p2a;
		delete cp1;
		delete cp2;
		return false;
	}
}

Interseccion* Triangulo::intersectaRayo(Rayo* rayoR){
	Vector* rayo = rayoR->ray;
	// Se chequea que el rayo no sea perpendicular al vector normal al plano.
	float cosNormRayo = this->norm->productoEscalar(rayo) / (this->norm->modulo() * rayo->modulo());
	if(!X_igualA_Y(cosNormRayo,0)) {
		// Se calcula el plano que contiene al triangulo (Ax+By+Cz+D=0) a partir del vector normal y un punto.
		float A = this->norm->resta->x;
		float B = this->norm->resta->y;
		float C = this->norm->resta->z;
		float D = -(A*this->p1->x + B*this->p1->y + C*this->p1->z);
		float lambda = - ((D+A*rayo->origen->x+B*rayo->origen->y+C*rayo->origen->z)/(A*rayo->resta->x+B*rayo->resta->y+C*rayo->resta->z));
		// Se utiliza el lambda para calcular la interseccion.
		Punto* intersec = new Punto(rayo->origen->x+lambda*rayo->resta->x, rayo->origen->y+lambda*rayo->resta->y, rayo->origen->z+lambda*rayo->resta->z);
		// Se chequea que est� adentro del triangulo usando "Barycentric Technique".
		if(mismoLado(intersec, this->p1, this->p3, this->p2) && mismoLado(intersec, this->p3, this->p1, this->p2) && mismoLado(intersec, p2, p1, p3)) {
			return new Interseccion(intersec, this);
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
}





		//Vector* v2 = new Vector(p1,intersec);
		//Vector* vectorialV0V2 = v0->productoVectorial(v2);
		//Vector* vectorialV1V2 = v1->productoVectorial(v2);
		//float invDenom = 1 / (vectorialV0V0->productoEscalar(vectorialV1V1) - vectorialV0V1->productoEscalar(vectorialV0V1));
		//float u = (vectorialV1V1->productoEscalar(vectorialV0V2) - vectorialV0V1->productoEscalar(vectorialV1V2)) * invDenom;
		//float v = (vectorialV0V0->productoEscalar(vectorialV1V2) - vectorialV0V1->productoEscalar(vectorialV0V2)) * invDenom;
		////delete v2;
		////delete vectorialV0V2;
		////delete vectorialV1V2;
		//if((u >= 0) && (v >= 0) && ((u+v) < 1)) {
		//	/*if (!X_mayorA_Y(((intersec->x - rayo->origen->x)/(rayo->resta->x)) , 0)){
		//		return NULL;
		//	}*/
		//	return new Interseccion(intersec, this);
		//} else {
		//	return NULL;
		//}
//	} else {
//		return NULL;
//	}
//}
